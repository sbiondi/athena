#include "TrigEgammaAnalysisTools/TrigEgammaNavAnalysisTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavTPAnalysisTool.h"
#include "../TrigEgammaMonAlgorithm.h"

DECLARE_COMPONENT(TrigEgammaMonAlgorithm)
DECLARE_COMPONENT(TrigEgammaNavAnalysisTool)
DECLARE_COMPONENT(TrigEgammaNavTPAnalysisTool)
