/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/


using namespace TrigCompositeUtils;

namespace Trig{


/*
 * Get the passed information for a given feature from TrigEgammaMatchingUtils::Element 
 */
template<class CONTAINER>
inline bool TrigEgammaMatchingTool::ancestorPassed( const TrigEgammaMatchingUtils::Element obj, const std::string key) const
{
  if( !obj.isValid())  return false;
  if( tdt()->getNavigationFormat() == "TriggerTrigEgammaMatchingUtils::Element" ){
    if ( obj.te() == nullptr ) return false;
    if ( (tdt()->ancestor<CONTAINER>( obj.te(),key)).te() == nullptr )
        return false;
    return ( (tdt()->ancestor<CONTAINER>(obj.te(),key)).te()->getActiveState());
  }else{
    if(obj.data() == nullptr)  return false;
    const TrigCompositeUtils::LinkInfo<CONTAINER> linkInfo = TrigCompositeUtils::findLink<CONTAINER>(obj.data(), "feature");
    if( ! linkInfo.isValid())  return false;
    const auto *fDec = linkInfo.source;
    HLT::Identifier id(obj.name());
    return  (std::find(fDec->decisions().begin(), fDec->decisions().end(), id.numeric()) != fDec->decisions().end());
  }
  return false;
}






/*
 * User method to get the closest required object given an pair offline and online objects
 */
template <class OBJECT>
inline const OBJECT* TrigEgammaMatchingTool::closestObject( std::pair<const xAOD::Egamma *, const TrigEgammaMatchingUtils::Element> pairObj, 
                                                            std::string key , float dRmax) const
{
  if(!pairObj.first ) return nullptr;
  float eta = pairObj.first->eta();
  float phi = pairObj.first->phi();
  // Reset to resonable start value
  if(dRmax < 0.15) dRmax = 0.15;
  TrigEgammaMatchingUtils::Container<const OBJECT*> vec = getFeatures<OBJECT>( pairObj.second, key );
  if(!vec.isValid()) return nullptr;
  const OBJECT *cl = nullptr;
  float dr=0.; 
  for(const auto& obj : vec.cptr()){
    if(obj==nullptr) continue;
    dr=dR(eta,phi,obj->eta(),obj->phi());
    if ( dr<dRmax){
      dRmax=dr;
      cl = obj;
    } // dR
  }
  return cl;
}


template<class OBJECT>
inline std::vector< TrigEgammaMatchingUtils::Feature<const OBJECT*>> TrigEgammaMatchingTool::features( std::string trigger, std::string key ) const
{
  std::vector< TrigEgammaMatchingUtils::Feature<const OBJECT*> > vec_features;
  if( tdt()->getNavigationFormat() == "TriggerTrigEgammaMatchingUtils::Element" ){
    Trig::FeatureContainer fc = tdt()->features(trigger);                        
    auto vec = fc.get<DataVector<OBJECT>>(key,TrigDefs::alsoDeactivateTEs);               
    for(auto feat : vec){                                                        
      const DataVector<OBJECT> *cont = feat.cptr();                                       
      if(cont == nullptr) continue;                                                                
      for(const auto& p : *cont){                                                
        if(p == nullptr)  continue;           

        TrigEgammaMatchingUtils::Element obj( trigger, feat.te() );
        vec_features.push_back( TrigEgammaMatchingUtils::Feature<const OBJECT*>(p, obj)  );
      }                                                                        
    }
  }else{
    auto vec =  tdt()->features<DataVector<OBJECT>>(trigger,TrigDefs::includeFailedDecisions ,key);      
    for( LinkInfo<DataVector<OBJECT>> &featLinkInfo : vec ){                                             
      const auto *feat = *(featLinkInfo.link);                                                  
      TrigEgammaMatchingUtils::Element obj( trigger, featLinkInfo.source );
      vec_features.push_back( TrigEgammaMatchingUtils::Feature<const OBJECT*>(feat, obj)  );
    }
  }
  return vec_features;
}




/*
 * Internal usage in the trigger e/g matching algorithm
 */
template<class OBJECT>
inline bool TrigEgammaMatchingTool::closestObject( const xAOD::Egamma *eg, TrigEgammaMatchingUtils::Element &obj, std::string trigger, std::string key ) const
{
  double deltaR=0.;        

  if( tdt()->getNavigationFormat() == "TriggerTrigEgammaMatchingUtils::Element" ){
    Trig::FeatureContainer fc = tdt()->features(trigger);                        
    auto vec = fc.get<DataVector<OBJECT>>(key,TrigDefs::alsoDeactivateTEs);               
    for(auto feat : vec){                                                        
      const DataVector<OBJECT> *cont = feat.cptr();                                       
      if(cont == nullptr) continue;                                                                
      for(const auto& p : *cont){                                                
        if(p == nullptr)  continue;                                                              
        deltaR = dR(eg->eta(),eg->phi(), p->eta(),p->phi());                     
        if(deltaR < m_dR){
          obj.setPtr( feat.te() );                                                 
          return true;                                                           
        }                                                                        
      }                                                                          
    }
  }else{
    auto vec =  tdt()->features<DataVector<OBJECT>>(trigger,TrigDefs::includeFailedDecisions ,key);      
    for( LinkInfo<DataVector<OBJECT>> &featLinkInfo : vec ){                                             
      const auto *feat = *(featLinkInfo.link);                                                  
      deltaR = dR(eg->eta(),eg->phi(), feat->eta(),feat->phi());                                
      if(deltaR < m_dR){            
        obj.setPtr( featLinkInfo.source );                                                
        return true;                                                                            
      }                                                                                         
    }
  }
  return false;
}




template<class OBJECT>
inline TrigEgammaMatchingUtils::Container<const OBJECT*> TrigEgammaMatchingTool::getFeatures( const TrigEgammaMatchingUtils::Element obj, std::string key) const
{
  TrigEgammaMatchingUtils::Container<const OBJECT*> vec;
  
  if( !obj.isValid() )  return vec;

  if( tdt()->getNavigationFormat() == "TriggerTrigEgammaMatchingUtils::Element" ){
    if ( obj.te() == nullptr ) return vec;
    if ( (tdt()->ancestor<DataVector<OBJECT>>( obj.te(), key)).te() == nullptr ) return vec; 
    const DataVector<OBJECT> *container = ( (tdt()->ancestor< DataVector<OBJECT> >(obj.te(),key)).cptr() );
    for (const OBJECT* object : *container) vec.push_back( object );

  }else{
    if ( obj.data() == nullptr ) return vec;
    auto initRoi = TrigCompositeUtils::findLink<TrigRoiDescriptorCollection>( obj.data(), "initialRoI");
    if( !initRoi.link.isValid() ) return vec;                                   
    auto features =  tdt()->features< DataVector<OBJECT> >( obj.name(), TrigDefs::includeFailedDecisions , key);
    for( LinkInfo< DataVector<OBJECT> > &featLinkInfo : features ){                                                       
      auto current_roi = TrigCompositeUtils::findLink<TrigRoiDescriptorCollection>(featLinkInfo.source, "initialRoI");
      if( ! current_roi.isValid()) continue;                                                              
      if( (*current_roi.link)->roiWord() == (*initRoi.link)->roiWord()) vec.push_back( *(featLinkInfo.link)  );                                                           
    }
  }

  return vec;                                                                                          
}


/*
 * Get the EmTauRoI object from TrigEgammaMatchingUtils::Element
 */
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::EmTauRoI*> TrigEgammaMatchingTool::getFeatures<xAOD::EmTauRoI>( const TrigEgammaMatchingUtils::Element obj, std::string key ) const
{
  TrigEgammaMatchingUtils::Container<const xAOD::EmTauRoI*> vec;
  if( !obj.isValid() )  return vec;
  if( tdt()->getNavigationFormat() == "TriggerTrigEgammaMatchingUtils::Element" ){
    if ( obj.te() == nullptr ) return vec;
    if ( (tdt()->ancestor<xAOD::EmTauRoI>( obj.te(), key)).te() == nullptr ) return vec; 
    vec.push_back( ( (tdt()->ancestor<xAOD::EmTauRoI>(obj.te(),key)).cptr() ) );
  }else{
    if ( obj.data() == nullptr ) return vec;
    auto initRoi = TrigCompositeUtils::findLink<TrigRoiDescriptorCollection>(obj.data(), "initialRoI"); 
    if( !initRoi.link.isValid() ) return vec;      
    const DataVector<xAOD::EmTauRoI> *emtau_cont = nullptr;
    if ( !evtStore()->retrieve( emtau_cont, "LVL1EmTauRoIs").isSuccess() ){
      ATH_MSG_WARNING("Failed to retrieve LVL1EmTauRoI container. Exiting.");
      return vec;
    }
    for( const auto *emtau : *emtau_cont ){
      if ( emtau->roiType() == xAOD::EmTauRoI::RoIType::EMRoIWord) continue;
      if((*initRoi.link)->roiWord()==emtau->roiWord()){
        vec.push_back(emtau);
      }
    }
  }
  return vec;
}


// *********************************************************************

/*
 * *** User methods to retrive features from an element ***
 */

// L1Calo
template<>
inline const xAOD::EmTauRoI* TrigEgammaMatchingTool::getFeature<xAOD::EmTauRoI>( const TrigEgammaMatchingUtils::Element obj ) const
{
  TrigEgammaMatchingUtils::Container<const xAOD::EmTauRoI*> vec = getFeatures<xAOD::EmTauRoI>( obj, key("L1Calo") );
  return ( !vec.isValid() ) ? nullptr : vec.cptr().front();
}


// L2Calo
template<>
inline const xAOD::TrigEMCluster* TrigEgammaMatchingTool::getFeature<xAOD::TrigEMCluster>( const TrigEgammaMatchingUtils::Element obj ) const
{
  TrigEgammaMatchingUtils::Container<const xAOD::TrigEMCluster*> vec = getFeatures<xAOD::TrigEMCluster>( obj, key("L2Calo") );
  return !vec.isValid() ? nullptr : vec.cptr().front();
}


// L2Electron
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::TrigElectron*> TrigEgammaMatchingTool::getFeatures<xAOD::TrigElectron>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::TrigElectron>( obj, key("L2Electron") );
}


// L2Photon
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::TrigPhoton*> TrigEgammaMatchingTool::getFeatures<xAOD::TrigPhoton>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::TrigPhoton>( obj, key("L2Photon") );
}


// Track
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::TrackParticle*> TrigEgammaMatchingTool::getFeatures<xAOD::TrackParticle>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::TrackParticle>( obj, key("Track") );
}


// CaloCluster
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::CaloCluster*> TrigEgammaMatchingTool::getFeatures<xAOD::CaloCluster>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::CaloCluster>( obj, key("EFCalo") );
}


// Electron
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::Electron*> TrigEgammaMatchingTool::getFeatures<xAOD::Electron>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::Electron>( obj, key("Electron") );
}


// Photon
template<>
inline TrigEgammaMatchingUtils::Container<const xAOD::Photon*> TrigEgammaMatchingTool::getFeatures<xAOD::Photon>( const TrigEgammaMatchingUtils::Element obj ) const
{
  return getFeatures<xAOD::Photon>( obj, key("Photon") );
}




// *********************************************************************

/*
 * *** User methods to retrive all features given a trigger ***
 */

// L2Calo
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::TrigEMCluster*>> TrigEgammaMatchingTool::features<xAOD::TrigEMCluster>( std::string trigger ) const
{
  return features<xAOD::TrigEMCluster>(trigger, key("L2Calo") );
}


// L2Electron
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::TrigElectron*>> TrigEgammaMatchingTool::features<xAOD::TrigElectron>( std::string trigger ) const
{
  return features<xAOD::TrigElectron>(trigger, key("L2Electron") );
}



// L2Photon
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::TrigPhoton*>> TrigEgammaMatchingTool::features<xAOD::TrigPhoton>( std::string trigger ) const
{
  return features<xAOD::TrigPhoton>(trigger, key("L2Photon") );
}


// CaloCluster
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::CaloCluster*>> TrigEgammaMatchingTool::features<xAOD::CaloCluster>( std::string trigger ) const
{
  return features<xAOD::CaloCluster>(trigger, key("EFCalo") );
}


// Electron
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::Electron*>> TrigEgammaMatchingTool::features<xAOD::Electron>( std::string trigger ) const
{
  return features<xAOD::Electron>(trigger, key("Electron") );
}



// Photon
template<>
inline std::vector< TrigEgammaMatchingUtils::Feature<const xAOD::Photon*>> TrigEgammaMatchingTool::features<xAOD::Photon>( std::string trigger ) const
{
  return features<xAOD::Photon>(trigger, key("Photon") );
}






}// namespace
