#include "CaloMonitoring/LArCellMonTool.h"
#include "CaloMonitoring/TileCalCellMonTool.h"
#include "CaloMonitoring/CaloClusterVecMon.h"
#include "CaloMonitoring/CaloTowerVecMon.h"
#include "CaloMonitoring/CaloTransverseBalanceVecMon.h"
#include "../CaloBaselineMonAlg.h"
#include "../TileCalCellMonAlg.h"

DECLARE_COMPONENT(TileCalCellMonTool)
DECLARE_COMPONENT(LArCellMonTool)
DECLARE_COMPONENT(CaloClusterVecMon)
DECLARE_COMPONENT(CaloTowerVecMon)
DECLARE_COMPONENT(CaloTransverseBalanceVecMon)
DECLARE_COMPONENT(CaloBaselineMonAlg)
DECLARE_COMPONENT(TileCalCellMonAlg)
