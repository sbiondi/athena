#
# File specifying the location of Lhapdf to use.
#

set( LHAPDF_LCGVERSION 6.2.3 )
set( LHAPDF_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/lhapdf/${LHAPDF_LCGVERSION}/${LCG_PLATFORM} )
